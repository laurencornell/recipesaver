import time
import pdfkit

from selenium import webdriver
from selenium.webdriver.common.by import By
from random import randint


def has_xpath(driver, xpath):
    try:
        driver.find_element(By.XPATH, xpath)
        return True
    except:
        return False


login_url = "https://www.cooksillustrated.com/sign_in?next=%2F"
email = ""
password = ""

driver = webdriver.Safari()
driver.get(login_url)

driver.find_element(By.ID, "email").send_keys(email)
driver.find_element(By.ID, "password").send_keys(password)
driver.find_element(By.XPATH, "//button[@class='appForm__submit']").click()
time.sleep(randint(1, 5))

i = 0
consecutive_errors = 0

while True:
    i += 1

    attempts = 0
    while True:
        try:
            driver.get('https://www.cooksillustrated.com/recipes/' + str(i))
            time.sleep(randint(1, 3))

            break
        except:
            print('Error thrown. Trying again')
            attempts += 1
            if attempts < 3:
                print('Attempted ' + str(i) + ' three times. Moving to next recipe.')
                attempts = 0
                i += 1

    if has_xpath(driver, "//div[@class='milk']"):
        print('Error on recipe ' + str(i))
        if i > 14500:
            consecutive_errors += 1
        """I don't know what the highest index is, but I know there are still recipes around 14400 so roll with it"""
        if consecutive_errors > 50:
            print('Exiting')
            break
        continue
    else:
        consecutive_errors = 0

    # if has_xpath(driver, "//a[@class='bx-close-link']"):
    #     driver.find_element(By.XPATH, "//a[@class='bx-close-link']").click()

    filename = driver.find_element(By.XPATH, "//div[@class='detail-page-main']").get_attribute('data-document-id')
    file = driver.find_element(By.XPATH, "//div[@id='recipe-detail__page-wrapper']").get_attribute('innerHTML')

    html_file = open('htmls/' + filename + '.html', 'w')
    html_file.write(file)
    html_file.close()
    pdfkit.from_string(file, 'pdfs/' + filename + '.pdf')
    print(filename + ' saved!')

driver.close()

